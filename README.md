﻿# Instalar_QE_Octave_Xcrysden

Este proyecto tiene como finalidad crear un script para disminuir el tiempo de instalación y configuración de los programa necesarios para hacer cálculos en la línea de Estudio de Nuevos Materiales, usados por el grupo de investigación CMT de la Universidad del Magdalena Santa Marta, Colombia.

## Paquetes a instalar:
1. Compiladores gcc y gfortran.
2. Librerías de algebra lineal BLAS, LAPACK, FFTW, SCALAPACK.
3. OpenMPI para la paralelización.
4. GNUPLOT para graficar.
5. Quantum-ESPRESSO.
6. Octave.
7. nano y tmux.
8. XCrySDen

Espero les sea de utilidad.

## Para ejecutar el script de instalación:
1. Debe tener privilegios de root y ejecutarlo siendo super usuario (sudo)
2. chmod u+xr install_QeOcXc.sh
3. ./install_QeOcXc.sh
4. Después de salir del root y estar en la cuenta de usuario en la cuál va a ejecutar los programas realizar: source .bashrc

>>>
##### Nota 1: 
Todos los programas del paquete de Quantum-ESPRESSO instalados en Fedora o CentOS tienen en su nombre "_openmpi", por ejemplo:
pw.x es pw.x_openmpi, ph.x es ph.x_openmpi. Para Ubuntu y Debian es normal pw.x, ph.x, etc.

Al definir los alias en el .bashrc, por ejemplo, alias pw.x='pw.x_openmpi' funciona cuando se ejecuta para un procesador,
por ejemplo, pw.x < inputfile.in > outputfile.out, sin embargo, al momento de correr dicho alias con mpirun no lo reconose,
por esta razón en este script no se definen los alias en el .bashrc, pero dejo un archivo para que la persona que los quiera
incluir lo pueda hacer. Para hacer esto debe seguir los mismos para que para ejecutar install_QeOcXc.sh sólo que en esta ocación
debe ejecutar ./alias_qe.sh
>>>

>>>
##### Nota 2: 
Por lo general para instalar programas o paquetes en Fedora, CentOS y Debian se debe ingresar al root, si usted a configurado su equipo para instalarlo todo con super usuario (sudo) para el casi de Debian al momento de la instalación debe seleccionar la opción de Ubuntu, para el caso de Fedora o CentOS es necesario editar el script ya sea con "sudo dnf" o "sudo yum".
>>>

## Realizar prueba:
Ingrese a la carpeta prueba, proceda a dar permisos de ejecución como anteriormente lo hizo con el archivo de instalación.

El scirpt de prueba (si_scf_phonon.sh) ejecutará un calculo Auto-consistente, uno de phonones en Gamma para el Si y desplegará
xcrysden mostrando la estructura descrita en el inputfile.
