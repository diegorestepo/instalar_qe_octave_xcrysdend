#!/bin/bash

# Programa para hacer el calculo scf, phonon.
# Inspirado en el ejemplo 2 de los ejemplos
# de Quentum ESPRESSO 4.1.1
# Escrito por Diego Andrés Restrepo Leal
# diegorestrepoleal@gmail.com

# LIMPIAR pantalla
clear

# Dirección de la ejecución del cálculo
DIR_CALCULO=`pwd`

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo
echo "$VERDE Inicio del calculo, dirección: $LIMPIAR"
echo "$VERDE $DIR_CALCULO $LIMPIAR"
echo

echo
echo "$AZUL ========================================================= $LIMPIAR"
echo "$AZUL = Ingrese el número de nucleos para ejecutar el cálculo = $LIMPIAR"
echo "$AZUL ========================================================= $LIMPIAR"
echo
read NUCLEOS

echo
echo "$AZUL ======================================================================= $LIMPIAR"
echo "$AZUL =    Eliga el sistema operativo en el que va a ejecutar el cálculo    = $LIMPIAR"
echo "$AZUL ======================================================================= $LIMPIAR"
echo "$LILA Digite:         1 = Fedora y CentOS,        2 = Ubtunu y Debian         $LIMPIAR"
echo "$AZUL ======================================================================= $LIMPIAR"
echo
read OS
case $OS in
	1)
		echo
		echo "$AZUL Fedora o CentOS $LIMPIAR"
		echo
		PW_COMMAND=pw.x_openmpi
		PH_COMMAND=ph.x_openmpi
 	;;
	2)
		echo
		echo "$AZUL Ubuntu o Debian $LIMPIAR"
		echo
		PW_COMMAND=pw.x
		PH_COMMAND=ph.x
	;;
esac

echo 
echo "$LILA Correr pw.x como:$LIMPIAR $PW_COMMAND"
echo "$LILA Correr ph.x como:$LIMPIAR $PH_COMMAND"
echo 

# Crear tmp
if [ -d tmp/ ];
then
	echo
	echo "Existe tmp"
	echo "$ROJO Borrar tmp para crear otra tmp $LIMPIAR"
	echo
	rm -rf tmp/
	mkdir tmp/

else
	echo
	echo "No existe tmp"
	echo "$VERDE Crear tmp $LIMPIAR"
	echo
	mkdir tmp
fi

# Comprobar CRASH
if [ -f CRASH ];
then
	echo "Existe el archivo CRASH"
	echo "$ROJO Eliminar archivo $LIMPIAR"
	rm CRASH
fi

# self-consistent calculation
cat > si.scf.in << EOF
 &control
    calculation='scf',
    restart_mode='from_scratch',
    prefix='si'
    pseudo_dir = './',
    outdir='./tmp'
 /
 &system
    ibrav = 2, celldm(1) =10.20, nat=  2, ntyp= 1,
    ecutwfc = 18.0
 /
 &electrons
    mixing_beta = 0.7
    conv_thr =  1.0d-8
 /
ATOMIC_SPECIES
 Si  28.086  Si.pz-vbc.UPF
ATOMIC_POSITIONS
 Si 0.00 0.00 0.00
 Si 0.25 0.25 0.25
K_POINTS
  10
   0.1250000  0.1250000  0.1250000   1.00
   0.1250000  0.1250000  0.3750000   3.00
   0.1250000  0.1250000  0.6250000   3.00
   0.1250000  0.1250000  0.8750000   3.00
   0.1250000  0.3750000  0.3750000   3.00
   0.1250000  0.3750000  0.6250000   6.00
   0.1250000  0.3750000  0.8750000   6.00
   0.1250000  0.6250000  0.6250000   3.00
   0.3750000  0.3750000  0.3750000   1.00
   0.3750000  0.3750000  0.6250000   3.00
EOF
echo
echo "$AZUL Correr scf para Si $LIMPIAR"
mpirun -np $NUCLEOS $PW_COMMAND -inp si.scf.in > si.scf.out
echo "$VERDE === Listo === $LIMPIAR"
echo

# phonon calculation at Gamma
cat > si.phG.in << EOF
phonons of Si at Gamma
 &inputph
  tr2_ph=1.0d-14,
  prefix='si',
  epsil=.true.,
  amass(1)=28.08,
  outdir='./tmp',
  fildyn='si.dynG',
 /
0.0 0.0 0.0
EOF
echo
echo "$AZUL Correr phonon en Gamma para Si $LIMPIAR"
mpirun -np $NUCLEOS $PH_COMMAND -inp si.phG.in > si.phG.out
echo "$VERDE === Listo === $LIMPIAR"
echo

echo "$AZUL Abrir  XCrySDen $LIMPIAR"
cp si.scf.in si.in
xcrysden --pwi si.in

exit 0
